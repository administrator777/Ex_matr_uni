﻿using System;
using System.Linq;

namespace ex._2
{
     internal class Program
     {

          public class NumberPosition
          {
               public int Number { get; set; }
               public int Position { get; set; }
          }

          public static void PrintDelimiter()
          {
               Console.WriteLine("*********************************************");
               Console.WriteLine();
          }

          public static void AllItemsAreZero(int[] arr)
          {
               var result = Array.TrueForAll(arr, y => y == 0);
               if (result)
               {
                    Console.WriteLine("The elements are equal to 0");
                    Console.ReadLine();
                    Environment.Exit(0);

               }
          }

          public static NumberPosition GetPositionNumberBySign(int[] arr, bool isPositive)
          {
               var findNum = false;
               var nr = int.MaxValue;
               var poz = 0;
               foreach (var i in arr)
               {
                    if ((isPositive && i > 0) || (!isPositive && i < 0))
                    {
                         findNum = true;
                         nr = i;
                         poz++;
                         break;
                    }
               }


               if (findNum != true)
               {
                    Console.WriteLine("Positive numbers not found ");
                    PrintDelimiter();
               }
               else
               {
                    return new NumberPosition()
                    {
                         Number = nr,
                         Position = poz
                    };
               }

               Console.ReadKey();
               return null;
          }

          public static NumberPosition FirstPositive(int[] arr)
          {
               var findNum = false;
               var nr = int.MaxValue;
               foreach (var i in arr)
                    if (i >= 0)
                    {
                         findNum = true;
                         nr = i;
                         break;
                    }

               var poz = Array.IndexOf(arr, nr);
               if (findNum != true)
               {
                    Console.WriteLine("Positive numbers not found ");
                    PrintDelimiter();
               }
               else
               {
                    return new NumberPosition()
                    {
                         Number = nr,
                         Position = poz
                    };

               }
               if (findNum)
               {
                    Console.WriteLine("Negative numbers not found");
                    PrintDelimiter();
               }
               else
               {
                    return new NumberPosition()
                    {
                         Number = nr,
                         Position = poz

                    };
               }
               Console.ReadKey();
               return null;
          }

          public static NumberPosition FirstNegative(int[] arr)
          {
               var findNumNeg = false;
               var nrNeg = int.MaxValue;
               foreach (var i in arr)
                    if (i < 0)
                    {
                         findNumNeg = true;
                         nrNeg = i;
                         break;
                    }
               /*
               var poz = Array.IndexOf(arr, nrNeg);
               if (findNumNeg != true)
               {
                    Console.WriteLine("Negative numbers not found");
                    PrintDelimiter();
               }
               else
               {
               }
               Console.ReadKey();*/
               return null;
          }


          public static NumberPosition OddElements(int[] arr)
          {
               var findNumNeg = 0;
               var oddElem = false;
               var oddNr = int.MaxValue;
               foreach (var i in arr)
               {
                    if (i % 2 != 0)
                    {
                         oddElem = true;
                         oddNr = i;
                    }

                    if (i < 0) findNumNeg++;
               }

               if (oddNr == int.MaxValue)
               {
                    Console.WriteLine("Odd number not found");
                    PrintDelimiter();
               }
               else
               {
                    var poz = Array.IndexOf(arr, oddNr);
                    Console.WriteLine("The last odd number is " + oddNr);
                    Console.WriteLine("his position is " + poz);
                    PrintDelimiter();
               }

               if (findNumNeg != 0)
               {
                    Console.WriteLine("The number of negative elements is " + findNumNeg);
                    PrintDelimiter();
               }
               else
               {
                    Console.WriteLine("Negative numbers not found");
                    PrintDelimiter();
               }
               Console.ReadKey();
               return null;
          }

          private static void Main(string[] args)
          {
               Console.WriteLine("Enter the number of items:");
               var n = int.Parse(Console.ReadLine());
               var arr = new int[n];
               Console.WriteLine("Enter the items by typing enter:");
               for (var i = 0; i < n; i++) arr[i] = int.Parse(Console.ReadLine());


               var firstPositiveNumberPosition = FirstPositive(arr);
               Console.WriteLine("The first positive element is " + firstPositiveNumberPosition.Number);
               Console.WriteLine("his position is " + firstPositiveNumberPosition.Position);
               // IsZero(arr);
               //FirstNegative(arr);
               //OddElements(arr);


               firstPositiveNumberPosition = GetPositionNumberBySign(arr, true);

               var firstNegativeNumberPosition = FirstNegative(arr);
               Console.WriteLine("The first negative number is" + firstNegativeNumberPosition.Number);
               Console.WriteLine("his position is " + firstNegativeNumberPosition.Position);

               firstNegativeNumberPosition = GetPositionNumberBySign(arr, false);


               PrintDelimiter();

               Console.ReadLine();
          }

     }
}